#include <gtest/gtest.h>
#include <data.h>

TEST(DataTest, CreateEmptyTest_Success)
{
    Data data;
    ASSERT_STREQ("", data.getData());
}


TEST(DataTest, SetGetTest_Success)
{
    const char* str1 = "String 1";

    Data data(str1);
    ASSERT_STREQ(str1, data.getData());

    const char* str2 = "String 2";

    data.setData(str2);
    ASSERT_STREQ(str2, data.getData());
}

TEST(DataTest, SaveLoadTest_Success)
{
    const char* str1 = "String 1";

    Data data(str1);
    ASSERT_TRUE(data.saveData());

    ASSERT_TRUE(data.loadData());

    ASSERT_STREQ(str1, data.getData());
}
