#ifndef DATA_H
#define DATA_H

#include <string>

class Data
{
public:
    Data();
    Data(const char* data);

    void setData(const char* data);
    const char* getData() const;

    bool saveData() const;
    bool loadData();

private:
    static const uint BUFF_LENGTH = 256;
    static const char* FILE_NAME;

    std::string m_data;
};

#endif // DATA_H
