#include "data.h"
#include <fstream>

const char* Data::FILE_NAME = "data.txt";

Data::Data()
{}

Data::Data(const char* data)
    : m_data(data)
{}

void Data::setData(const char* data)
{
    m_data = data;
}

const char* Data::getData() const
{
    return m_data.c_str();
}

bool Data::saveData() const
{
    bool res = false;

    std::ofstream file;
    file.open(FILE_NAME, std::ios_base::trunc);

    if (file.is_open() == false)
    {
        return res;
    }

    file << m_data;

    file.close();

    res = true;

    return res;
}

bool Data::loadData()
{
    bool res = false;

    std::ifstream file;
    file.open(FILE_NAME);

    if (file.is_open() == false)
    {
        return res;
    }

    char buff[BUFF_LENGTH] = {0};
    file.getline(buff, BUFF_LENGTH);

    m_data = buff;

    res = true;

    return res;
}
