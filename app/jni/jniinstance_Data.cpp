#include "jniinstance_Data.h"

#include <iostream>
#include "data.h"

Data	g_data;

JNIEXPORT void JNICALL Java_jniinstance_Data_init
  (JNIEnv *, jobject)
{
    std::cout << __func__ << std::endl;
}

JNIEXPORT void JNICALL Java_jniinstance_Data_setData
  (JNIEnv* env, jobject, jstring jstr)
{
    const char* str = env->GetStringUTFChars(jstr, nullptr);

    g_data.setData(str);

    env->ReleaseStringUTFChars(jstr, str);
}

JNIEXPORT jstring JNICALL Java_jniinstance_Data_getData
  (JNIEnv* env, jobject)
{
    return env->NewStringUTF(g_data.getData());
}
