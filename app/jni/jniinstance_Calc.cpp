#include "jniinstance_Calc.h"

#include <iostream>

extern "C"
{
#include "calc.h"
}

JNIEXPORT void JNICALL Java_jniinstance_Calc_init
  (JNIEnv *, jobject)
{
    std::cout << __func__ << std::endl;
}

JNIEXPORT jdouble JNICALL Java_jniinstance_Calc_sum
  (JNIEnv *, jobject, jdouble ja, jdouble jb)
{
    return sum(ja, jb);
}

JNIEXPORT jdouble JNICALL Java_jniinstance_Calc_sub
  (JNIEnv *, jobject, jdouble ja, jdouble jb)
{
    return sub(ja, jb);
}

JNIEXPORT jdouble JNICALL Java_jniinstance_Calc_multiply
  (JNIEnv *, jobject, jdouble ja, jdouble jb)
{
    return multiply(ja, jb);
}

JNIEXPORT jdouble JNICALL Java_jniinstance_Calc_divide
  (JNIEnv *, jobject, jdouble ja, jdouble jb)
{
    return divide(ja, jb);
}
