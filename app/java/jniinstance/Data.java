package jniinstance;

public class Data
{
    public Data() {
        System.out.println("Data()");
    }

    public native void init();

    public native void setData(String data);
    public native String getData();
}
