package jniinstance;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileOutputStream;

import jniinstance.Data;
import jniinstance.Calc;

public class Main
{
    static {
        try {        
            // Prepare temporary file
            File temp = File.createTempFile("libapp-jni", ".tmp");
            temp.deleteOnExit();

            // Prepare buffer for data copying
            byte[] buffer = new byte[1024];
            int readBytes;
     
            // Init stream from resource file with jni lib
            InputStream is = Main.class.getResourceAsStream("/lib/libapp-jni.so");
            
            // Init stream for temp file
            OutputStream os = new FileOutputStream(temp);

            // Copy data of jni lib from recources to tmp file
            try {
                while ((readBytes = is.read(buffer)) != -1) {
                    os.write(buffer, 0, readBytes);
                }
            } finally {
                // If read/write fails, close streams safely before throwing an exception
                os.close();
                is.close();
            }

            // Finally, load the library
            System.load(temp.getAbsolutePath());
        } catch(IOException e) {
           e.printStackTrace();
        }
    }

    public static void main(String[] args)
    {
        if (args.length < 1)
        {
            System.out.println("No arguments inputed");
            return;
        }

        switch (args[0])
        {
            case "calc":
            {
                Calc calc = new Calc();
                calc.init();

                // TODO: Check existence of 1 and 2 args
                int a = Integer.parseInt(args[1]);
                int b = Integer.parseInt(args[2]);

                System.out.println("sum=" + calc.sum(a, b));
                System.out.println("sub=" + calc.sub(a, b));
                System.out.println("mul=" + calc.multiply(a, b));
                System.out.println("div=" + calc.divide(a, b));

                break;
            }
        
            case "data":
            {
                Data data = new Data();
                data.init();

                // TODO: Check existence inputed argument                
                data.setData(args[1]);

                System.out.println("data=" + data.getData());
            }

            default:
                System.out.println("Error: wrong input");
        }
    }
}
