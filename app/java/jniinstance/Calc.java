package jniinstance;

public class Calc
{
    public Calc() {
        System.out.println("Calc()");
    }

    public native void init();

    public native double sum(double a, double b);
    public native double sub(double a, double b);

    public native double multiply(double a, double b);
    public native double divide(double a, double b);
}
