#include <gtest/gtest.h>

TEST(JavaAppTest, NoArgsTest_Success)
{
    ASSERT_EQ(0, system("java -Djava.library.path=.. -jar ../app.jar | grep 'No arguments inputed'"));
}

TEST(JavaAppTest, ChooseCalcTest_Success)
{
    ASSERT_EQ(0, system("java -Djava.library.path=.. -jar ../app.jar calc 0 0 | grep 'Calc'"));

    ASSERT_EQ(0, system("java -Djava.library.path=.. -jar ../app.jar calc 2 3 | grep 'sum=5'"));
    ASSERT_EQ(0, system("java -Djava.library.path=.. -jar ../app.jar calc 3 1 | grep 'sub=2'"));
    ASSERT_EQ(0, system("java -Djava.library.path=.. -jar ../app.jar calc 3 4 | grep 'mul=12'"));
    ASSERT_EQ(0, system("java -Djava.library.path=.. -jar ../app.jar calc 10 2 | grep 'div=5'"));
}

TEST(JavaAppTest, ChooseDataTest_Success)
{
    ASSERT_EQ(0, system("java -Djava.library.path=.. -jar ../app.jar data 0 | grep 'Data'"));

    ASSERT_EQ(0, system("java -Djava.library.path=.. -jar ../app.jar data Text | grep 'Text'"));
}